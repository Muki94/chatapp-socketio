const path = require('path');
const express = require("express");

var app = express();
const port = process.env.PORT || 3000;
const publicPath = path.join(__dirname, "../public");

app.use(express.static(publicPath));

app.listen(port, function () {
    console.log(`Server started on port ${port}`);
});